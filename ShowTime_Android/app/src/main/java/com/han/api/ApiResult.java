package com.han.api;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author JosephT
 */
public class ApiResult {

    private List<ApiError> errors = new ArrayList<ApiError>();
    private String response;

    public ApiResult(String response) {
        this.response = response;
    }

    public ApiResult(List<ApiError> errors) {
        this.errors = errors;
    }

    public boolean isSuccessful() {
        return errors.isEmpty();
    }

    public List<ApiError> getErrors() {
        return errors;
    }

    public String getResponse() {
        return response;
    }

    public static ApiResult fromErrors(String errorsJson) throws JSONException {
        JSONObject object = new JSONObject(errorsJson);
        List<ApiError> errors = new ArrayList<ApiError>();
        JSONArray errorArray = object.getJSONArray("errors");
        for (int i = 0; i < errorArray.length(); i++) {
            errors.add(new ApiError(errorArray.getJSONObject(i).getString("message"), errorArray.getJSONObject(i).getString("code")));
        }

        return new ApiResult(errors);
    }
}


package com.han.api;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.han.tripican.R;
import com.han.utility.DialogUtility;

public class MyAsyncTask extends AsyncTask<String, Integer, ParseResult> {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public Context parent;
	public ProgressDialog dlgLoading;
	public String title;
	
	public String curAction;

	@Override
	protected ParseResult doInBackground(String... arg0) {
		// TODO Auto-generated method stub
		return new ParseResult(false);
	}
	
	public MyAsyncTask(Context _parent) {
		parent = _parent;
		title = parent.getResources().getString(R.string.please_wait);
	}
	
	public MyAsyncTask(Context _parent, String _title) {
		parent = _parent;
		title = _title;
	}
	
	@Override
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage(title);
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
    	
    	if (title != null) {
    		dlgLoading.show();
    	}
	}
	
	protected void onPostExecute(ParseResult pResult) {
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
		
		if (!pResult.isSuccess) {
			DialogUtility.show(parent, pResult.message);
		}
	}
	
	protected void onProgressUpdate(Integer... progress) {
		
	}
}

package com.han.api;

public class ParseResult {

	public String message = "";
    public boolean isSuccess = false;

	public ParseResult() {
        isSuccess = false;
		message = "";
	}
	
	public ParseResult(boolean _isSuccess) {
		isSuccess = _isSuccess;
	}
	
	public ParseResult(boolean _isSuccess, String _message) {
		isSuccess = _isSuccess;
		message = _message;
	}

	public void setResult(boolean _isSuccess, String _message) {
		isSuccess = _isSuccess;
		message = _message;
	}
}
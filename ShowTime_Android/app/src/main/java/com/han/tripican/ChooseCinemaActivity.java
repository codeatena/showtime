package com.han.tripican;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.han.tripican.model.Cinema;
import com.han.utility.PreferenceUtility;

import java.util.ArrayList;

public class ChooseCinemaActivity extends ActionBarActivity {

	Spinner spnCinemas;
	Button btnDone;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_cinema);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		spnCinemas = (Spinner) findViewById(R.id.cinemas_spinner);
		btnDone = (Button) findViewById(R.id.done_button); 
	}
	
	private void initValue() {

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        getSupportActionBar().setIcon(R.drawable.ic_main);

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#e28846'> Tripican Check IN</font>"));

		ArrayList<String> list = new ArrayList<String>();
		list.add("Select Location");
		
		for (Cinema cinema: GlobalData.arrCinemas) {
			list.add(cinema.centre_name);
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
			android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnCinemas.setAdapter(dataAdapter);
	}
	
	private void initEvent() {
		spnCinemas.setOnItemSelectedListener(new OnItemSelectedListener() {
			
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
				// TODO Auto-generated method stub
				if (position == 0) {
					btnDone.setEnabled(false);
				} else {
					btnDone.setEnabled(true);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
				// TODO Auto-generated method stub
				btnDone.setEnabled(false);
			}
	    });
		btnDone.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				int position = spnCinemas.getSelectedItemPosition();
				Cinema cinema = GlobalData.arrCinemas.get(position - 1);
				PreferenceUtility.getInstance().saveCinemaConfig(cinema);
				startActivity(new Intent(ChooseCinemaActivity.this, CinemaActivity.class));
				finish();
			}
        });
	}
}
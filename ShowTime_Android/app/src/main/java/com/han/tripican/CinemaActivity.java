package com.han.tripican;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.han.utility.PreferenceUtility;
import com.han.zxing.SimpleScannerFragmentActivity;

public class CinemaActivity extends ActionBarActivity {

	TextView txtCenterName;
	Button btnScan;
	Button btnEnterCode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cinema);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		txtCenterName = (TextView) findViewById(R.id.center_name_textView);
		btnScan = (Button) findViewById(R.id.scan_button);
		btnEnterCode = (Button) findViewById(R.id.enter_code_button);
	}
	
	private void initValue() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        getSupportActionBar().setIcon(R.drawable.ic_main);

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#e28846'> Tripican Check IN</font>"));
		txtCenterName.setText(PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CONFIG_CINEMA_CENTER_NAME, ""));
	}
	
	private void initEvent() {
		btnScan.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(CinemaActivity.this, SimpleScannerFragmentActivity.class));
			}
        });
		btnEnterCode.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
                startActivity(new Intent(CinemaActivity.this, CodeActivity.class));
			}
        });
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cinema, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            PreferenceUtility.getInstance().clearAllInf();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//            finishAffinity();
//            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

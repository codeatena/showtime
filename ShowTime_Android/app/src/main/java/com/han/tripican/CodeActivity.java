package com.han.tripican;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.han.tripican.api.asynctask.UserAsyncTask;
import com.han.tripican.api.parseresult.VerifyParseResult;
import com.han.utility.PreferenceUtility;

public class CodeActivity extends ActionBarActivity {

    TextView txtCenterName;
    EditText edtConfirmCode;

    Button btnEnter;
    Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {

        txtCenterName = (TextView) findViewById(R.id.center_name_textView);
        edtConfirmCode = (EditText) findViewById(R.id.confirm_code_editText);

        btnEnter = (Button) findViewById(R.id.enter_button);
        btnCancel = (Button) findViewById(R.id.cancel_button);
    }

    private void initValue() {

        txtCenterName.setText(PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CONFIG_CINEMA_CENTER_NAME, ""));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        getSupportActionBar().setIcon(R.drawable.ic_main);

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#e28846'> Tripican Check IN</font>"));
    }

    private void initEvent() {
        btnEnter.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = edtConfirmCode.getText().toString();
                new UserAsyncTask(CodeActivity.this).execute(UserAsyncTask.ACTION_VERIFY_TICKET, code);
            }
        });

        btnCancel.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void successVerify(VerifyParseResult verifyParseResult) {
        if (verifyParseResult.ticket_order.checkin_status == 0) {
            ValidActivity.ticketOrder = verifyParseResult.ticket_order;
            startActivity(new Intent(this, ValidActivity.class));
        }
        if (verifyParseResult.ticket_order.checkin_status == 1) {
            if (verifyParseResult.statusPass == VerifyParseResult.PASSED_CHECK_IN) {
                ValidActivity.ticketOrder = verifyParseResult.ticket_order;
                startActivity(new Intent(this, ValidActivity.class));
            } else {
                InvalidActivity.ticketOrder = verifyParseResult.ticket_order;
                startActivity(new Intent(this, InvalidActivity.class));
            }
        }
        finish();
    }
}

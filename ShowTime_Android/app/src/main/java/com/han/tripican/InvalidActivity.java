package com.han.tripican;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.han.tripican.model.Ticket;
import com.han.tripican.model.TicketOrder;
import com.han.utility.PreferenceUtility;

public class InvalidActivity extends ActionBarActivity {

    TextView txtTitle;
    TextView txtCenterName;
    TextView txtDate;
    TextView txtTime;
    TextView txtAdmits;
    TextView txtPrices;
    TextView txtOwner;
    TextView txtType;

    Button btnTryAgain;
    public static TicketOrder ticketOrder = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invalid);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        btnTryAgain = (Button) findViewById(R.id.try_again_button);

        txtTitle = (TextView) findViewById(R.id.title_textView);
        txtCenterName = (TextView) findViewById(R.id.center_name_textView);
        txtDate = (TextView) findViewById(R.id.date_textView);
        txtTime = (TextView) findViewById(R.id.time_textView);
        txtAdmits = (TextView) findViewById(R.id.admits_textView);
        txtPrices = (TextView) findViewById(R.id.price_textView);
        txtOwner = (TextView) findViewById(R.id.owner_textView);
        txtType = (TextView) findViewById(R.id.type_textView);
    }

    private void initValue() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        getSupportActionBar().setIcon(R.drawable.ic_main);

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#e28846'> Tripican Check IN</font>"));

        txtTitle.setText(ticketOrder.movie.title);
        txtCenterName.setText(ticketOrder.cinema.centre_name);

        txtDate.setText(ticketOrder.showTime.event_date);
        txtTime.setText(ticketOrder.showTime.event_time);

        txtType.setText(ticketOrder.showTime.showtime_type);
        txtOwner.setText(ticketOrder.user.email);

        int i = 0;
        String strAdults = "";
        String strPrice = "";

        for (Ticket ticket: ticketOrder.arrTickets) {
            if (i == 0) {
                strAdults = strAdults + ticket.quantity + " " + ticket.ticketCategory.name;
                strPrice = strPrice + "N" + ticket.unit_price;
            } else {
                strAdults = strAdults + "\n" + ticket.quantity + " " + ticket.ticketCategory.name;
                strPrice = strPrice + "\nN" + ticket.unit_price;
            }
            i++;
        }

        txtAdmits.setText(strAdults);
        txtPrices.setText(strPrice);
    }

    private void initEvent() {
        btnTryAgain.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_invalid, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            PreferenceUtility.getInstance().clearAllInf();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//            finishAffinity();
//            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

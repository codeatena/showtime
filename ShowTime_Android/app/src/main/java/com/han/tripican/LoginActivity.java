package com.han.tripican;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.han.tripican.api.asynctask.UserAsyncTask;
import com.han.utility.PreferenceUtility;

public class LoginActivity extends Activity {

	EditText edtEamil;
	EditText edtPassword;
    RelativeLayout rlytLoginFaield;

    ImageView imgCloseButton;

	Button btnLogin;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		edtEamil = (EditText) findViewById(R.id.email_editText);
		edtPassword = (EditText) findViewById(R.id.password_editText);
        rlytLoginFaield = (RelativeLayout) findViewById(R.id.login_failed_relativeLayout);
		btnLogin = (Button) findViewById(R.id.login_button);

//        edtPassword.setTypeface(Typeface.DEFAULT);
//        edtPassword.setTransformationMethod(new PasswordTransformationMethod());

        imgCloseButton = (ImageView) findViewById(R.id.title_textView);
	}
	
	private void initValue() {
        rlytLoginFaield.setVisibility(View.GONE);
//		edtEamil.setText("hcw1987@hotmail.com");
//		edtPassword.setText("hanzhong119");
	}
	
	private void initEvent() {
		btnLogin.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				String email = edtEamil.getText().toString();
				String password = edtPassword.getText().toString();
				new UserAsyncTask(LoginActivity.this).execute(UserAsyncTask.ACTION_LOGIN, email, password);
			}
         });
        imgCloseButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlytLoginFaield.setVisibility(View.GONE);
            }
        });
	}
	
	public void successLogin() {
		String email = edtEamil.getText().toString();
		String password = edtPassword.getText().toString();

        rlytLoginFaield.setVisibility(View.GONE);
		PreferenceUtility.getInstance().saveUserInf(email, password);
		startActivity(new Intent(this, ChooseCinemaActivity.class));
		finish();
	}

    public void failedLogin() {
        rlytLoginFaield.setVisibility(View.VISIBLE);
    }
}
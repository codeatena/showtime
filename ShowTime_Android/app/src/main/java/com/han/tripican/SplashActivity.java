package com.han.tripican;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.han.tripican.api.asynctask.UserAsyncTask;
import com.han.utility.PreferenceUtility;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

        initWidget();
        initValue();
        initEvent();
	}

    private void initWidget() {

    }

    private void initValue() {
        if (PreferenceUtility.getInstance().isSavedUserInf()) {
            String email = PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_EMAIL, "");
            String password = PreferenceUtility.getInstance().sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_PASSWORD, "");

            new UserAsyncTask(this, null).execute(UserAsyncTask.ACTION_LOGIN, email, password);
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }

    public void successLogin() {
        if (PreferenceUtility.getInstance().isSavedCinemaConfig()) {
            startActivity(new Intent(this, CinemaActivity.class));
            finish();
        } else {
            startActivity(new Intent(this, ChooseCinemaActivity.class));
            finish();
        }
    }

    private void initEvent() {

    }
}

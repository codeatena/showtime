package com.han.tripican.api.asynctask;

import android.content.Context;

import com.han.api.MyAsyncTask;
import com.han.api.ParseResult;
import com.han.tripican.CodeActivity;
import com.han.tripican.GlobalData;
import com.han.tripican.LoginActivity;
import com.han.tripican.SplashActivity;
import com.han.tripican.api.parseresult.CinemasParseResult;
import com.han.tripican.api.parseresult.VerifyParseResult;
import com.han.tripican.api.service.UserWebService;
import com.han.utility.DialogUtility;
import com.han.zxing.SimpleScannerFragmentActivity;

public class UserAsyncTask extends MyAsyncTask {

	public static String ACTION_LOGIN = "action_login";
	public static String ACTION_READ_CINEMAS = "action_read_cinemas";
    public static String ACTION_VERIFY_TICKET = "action_verify_ticket";
    public static String ACTION_CHECK_IN = "action_check_in";

    public UserAsyncTask(Context _parent) {
		super(_parent);
		// TODO Auto-generated constructor stub
	}

	public UserAsyncTask(Context _parent, String _title) {
		super(_parent, _title);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected ParseResult doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		ParseResult pResult = new ParseResult(false);

		curAction = params[0];
		if (curAction.equals(ACTION_LOGIN)) {
			pResult = UserWebService.login(params[1], params[2]);
			if (pResult.isSuccess) {
				pResult = UserWebService.readMyCinemas();
			}
		}
		if (curAction.equals(ACTION_READ_CINEMAS)) {
			pResult = UserWebService.readMyCinemas();
		}
        if (curAction.equals(ACTION_VERIFY_TICKET)) {
            pResult = UserWebService.verifyTicket(params[1]);
            if (pResult.isSuccess) {
                VerifyParseResult verifyParseResult = (VerifyParseResult) pResult;
                if (verifyParseResult.ticket_order.checkin_status == 0) {
                    pResult = UserWebService.checkInTicket(Integer.toString(verifyParseResult.ticket_order.id));
                }
            }
        }
		return pResult;
	}
	
	protected void onPostExecute(ParseResult pResult) {
		
		super.onPostExecute(pResult);
		
		if (curAction.equals(ACTION_LOGIN)) {
			if (pResult.isSuccess) {
                if (parent instanceof LoginActivity) {
                    LoginActivity loginActivity = (LoginActivity) parent;
                    CinemasParseResult cinemasParseResult = (CinemasParseResult) pResult;
                    GlobalData.arrCinemas = cinemasParseResult.arrCinemas;
                    loginActivity.successLogin();
                }
                if (parent instanceof SplashActivity) {
                    SplashActivity splashActivity = (SplashActivity) parent;
                    CinemasParseResult cinemasParseResult = (CinemasParseResult) pResult;
                    GlobalData.arrCinemas = cinemasParseResult.arrCinemas;
                    splashActivity.successLogin();
                }
			}
            else {
                LoginActivity loginActivity = (LoginActivity) parent;
                loginActivity.failedLogin();
            }
		}
		if (curAction.equals(ACTION_READ_CINEMAS)) {
            if (pResult.isSuccess) {
                LoginActivity loginActivity = (LoginActivity) parent;
                CinemasParseResult cinemasParseResult = (CinemasParseResult) pResult;
                GlobalData.arrCinemas = cinemasParseResult.arrCinemas;
                loginActivity.successLogin();
            }
		}
        if (curAction.equals(ACTION_VERIFY_TICKET)) {
            if (pResult.isSuccess) {
                if (parent instanceof CodeActivity) {
                    CodeActivity codeActivity = (CodeActivity) parent;
                    VerifyParseResult verifyParseResult = (VerifyParseResult) pResult;
                    codeActivity.successVerify(verifyParseResult);
                }
                if (parent instanceof SimpleScannerFragmentActivity) {
                    SimpleScannerFragmentActivity simpleScannerFragmentActivity = (SimpleScannerFragmentActivity) parent;
                    VerifyParseResult verifyParseResult = (VerifyParseResult) pResult;
                    simpleScannerFragmentActivity.successVerify(verifyParseResult);
                }
            } else {
                DialogUtility.showGeneralAlert(parent, "Error", pResult.message);
            }
        }
	}
}

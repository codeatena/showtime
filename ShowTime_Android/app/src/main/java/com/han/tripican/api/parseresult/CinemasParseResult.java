package com.han.tripican.api.parseresult;

import com.han.api.ParseResult;
import com.han.tripican.model.Cinema;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CinemasParseResult extends ParseResult {

	public ArrayList<Cinema> arrCinemas = new ArrayList<Cinema>();

    public CinemasParseResult parse(String strResponse) {

        CinemasParseResult pResult = new CinemasParseResult();
        try {
            JSONObject jsonObj = new JSONObject(strResponse);
            boolean isSuccess = (jsonObj.getInt("success") == 1) ? true: false;
            pResult.setResult(isSuccess, "json parsing error");

            if (pResult.isSuccess) {
                JSONArray jsonArrCinemas = jsonObj.getJSONArray("cinemas");
                for (int i = 0; i < jsonArrCinemas.length(); i++) {
                    JSONObject jsonCinema = jsonArrCinemas.getJSONObject(i);
                    Cinema cinema = new Cinema().parse(jsonCinema);
                    pResult.arrCinemas.add(cinema);
                }
                pResult.message = "success read cinemas";
            }

            return pResult;
        } catch (Exception e) {
            e.printStackTrace();
            pResult.setResult(false, "json parsing error");
            return pResult;
        }
    }
}
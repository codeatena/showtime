package com.han.tripican.api.parseresult;

import com.han.api.ParseResult;
import com.han.tripican.model.TicketOrder;

import org.json.JSONArray;
import org.json.JSONObject;

public class VerifyParseResult extends ParseResult {

    public static final int PASSED_NONE = 0;
    public static final int PASSED_VERIFY = 1;
    public static final int PASSED_CHECK_IN = 2;

    public TicketOrder ticket_order;
    public int statusPass = PASSED_NONE;

    public VerifyParseResult parse(String strResponse) {
        try {
            JSONObject jsonObj = new JSONObject(strResponse);
            isSuccess = (jsonObj.getInt("success") == 1) ? true: false;
            if (isSuccess) {
                ticket_order = new TicketOrder().parse(jsonObj.getJSONObject("ticket_order"));
            } else {
                ticket_order = null;
                JSONArray jsonErrors = jsonObj.getJSONArray("errors");
                JSONObject jsonError = jsonErrors.getJSONObject(0);
                message = jsonError.getString("message");
            }

            return this;
        } catch (Exception e) {
            e.printStackTrace();
            setResult(false, "json parsing error");
            return this;
        }
    }
}
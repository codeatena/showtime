package com.han.tripican.api.service;

import android.util.Log;

import com.han.api.MyWebService;
import com.han.api.OAuthToken;
import com.han.api.ParseResult;
import com.han.tripican.api.parseresult.CinemasParseResult;
import com.han.tripican.api.parseresult.VerifyParseResult;
import com.han.utility.PreferenceUtility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserWebService extends MyWebService {
	
    private static long ACCESS_TOKEN_TTL = 86400 * 60; //expiry of access token in seconds

    public static ParseResult checkInTicket(String ticket_id) {
        VerifyParseResult verifyParseResult = new VerifyParseResult();
        try {
            String url = URL_API + "staff/checkin.json";
            Log.e("Checkin url: ", url);
            OAuthToken oauthToken = PreferenceUtility.getInstance().getOAuthToken();

            Log.e("ticket_id", ticket_id);
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ticket_id", ticket_id));

            String strResponse = post(url, nameValuePairs, oauthToken);
            Log.e("Checkin Result: ", strResponse);

            if (verifyParseResult.parse(strResponse).isSuccess) {
                verifyParseResult.statusPass = VerifyParseResult.PASSED_CHECK_IN;
            }

            return verifyParseResult;
        } catch (Exception e) {
            e.printStackTrace();
            verifyParseResult.setResult(false, "json parsing error");
            return verifyParseResult;
        }
    }

    public static ParseResult verifyTicket(String code) {
        VerifyParseResult verifyParseResult = new VerifyParseResult();
        try {
            String url = URL_API + "staff/verify-ticket.json?" + "confirmation_code=" + code;
            Log.e("verify url: ", url);
            OAuthToken oauthToken = PreferenceUtility.getInstance().getOAuthToken();

            String strResponse = MyWebService.get(url, oauthToken);
            Log.e("Verify Result: ", strResponse);

            if (verifyParseResult.parse(strResponse).isSuccess) {
                verifyParseResult.statusPass = VerifyParseResult.PASSED_VERIFY;
            }

            return verifyParseResult;
        } catch (Exception e) {
            e.printStackTrace();
            verifyParseResult.setResult(false, "json parsing error");
            return verifyParseResult;
        }
    }

    public static ParseResult readMyCinemas() {

        CinemasParseResult cinemasParseResult = new CinemasParseResult();
    	try {
        	String url = URL_API + "staff/my-cinemas.json";
        	OAuthToken oauthToken = PreferenceUtility.getInstance().getOAuthToken();
        	
        	String strResponse = MyWebService.get(url, oauthToken);
            Log.e("My Cinemas: ", strResponse);
            
            return cinemasParseResult.parse(strResponse);
    	} catch (Exception e) {
    		e.printStackTrace();
            cinemasParseResult.setResult(false, "json parsing error");
    		return cinemasParseResult;
    	}
    }
    
	public static ParseResult login(String email, String password) {
		
		Log.e("method", "login");
		
		OAuthToken oauthToken = getAccessToken(email, password);
		ParseResult pResult = null;
		
		if (oauthToken == null) {
			pResult = new ParseResult(false, "Incorrect Username or Password");
		} else {
			PreferenceUtility.getInstance().saveOAuthToken(oauthToken);
			pResult = new ParseResult(true, "success");
		}
		
		return pResult;
	}
	
	public static OAuthToken getAccessToken(String email, String password) {
		
		try {			
			String requestTokenUrl = URL_API + "oauth/request_token";
			Log.e("Request Token url: ", requestTokenUrl);

			String reqToken = MyWebService.get(requestTokenUrl, null);
	        Log.e("Request Token Resp: ", reqToken);

	        Map<String, String> tokens = splitQuery(reqToken);

	        OAuthToken requestToken = new OAuthToken(tokens.get("oauth_token"), tokens.get("oauth_token_secret"));
	        
	        String accessTokenUrl = URL_API + "oauth/access_token";
			Log.e("Access Token url: ", accessTokenUrl);

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("email", email));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            nameValuePairs.add(new BasicNameValuePair("xoauth_token_ttl", Long.toString(ACCESS_TOKEN_TTL)));

	        String resp = post(accessTokenUrl, nameValuePairs, requestToken);
	        Log.e("Access Token Response: ", resp);

	        tokens = splitQuery(resp);

	        OAuthToken accessToken = new OAuthToken(tokens.get("oauth_token"), tokens.get("oauth_token_secret"));
	        Log.e("Access token: ", accessToken.toString());
	        
	        return accessToken;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
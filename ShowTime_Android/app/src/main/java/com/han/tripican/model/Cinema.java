package com.han.tripican.model;

import org.json.JSONObject;

public class Cinema {
    public int id;
    public String centre_name;
//    public String address_line_1;
//    public String address_line_2;

    public Cinema parse(JSONObject jsonObj) {
        try {
            id = jsonObj.getInt("id");
            centre_name = jsonObj.getString("centre_name");
            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
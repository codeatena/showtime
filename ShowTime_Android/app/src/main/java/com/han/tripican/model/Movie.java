package com.han.tripican.model;

import org.json.JSONObject;

/**
 * Created by COREI3 on 2/6/2015.
 */
public class Movie {

    public int id;
    public String title;
    public String description;

    public Movie parse(JSONObject jsonObj) {
        try {
            id = jsonObj.getInt("id");
            title = jsonObj.getString("title");
            description = jsonObj.getString("description");
            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
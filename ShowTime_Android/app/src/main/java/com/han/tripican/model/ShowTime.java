package com.han.tripican.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by COREI3 on 2/6/2015.
 */
public class ShowTime {

    public String event_date;
    public String event_time;
    public String type;
    public String showtime_type;
    public ArrayList<TicketPrice> arrTicketPrice;

    public ShowTime parse(JSONObject jsonObj) {
        try {
            event_date = jsonObj.getString("event_date");
            event_time = jsonObj.getString("event_time");
            type = jsonObj.getString("type");
            showtime_type = jsonObj.getString("showtime_type");

            JSONArray jsonArrTicketPrices = jsonObj.getJSONArray("ticket_prices");
            arrTicketPrice = new ArrayList<TicketPrice>();

            for (int i = 0; i < jsonArrTicketPrices.length(); i++) {
                TicketPrice ticketPrice = new TicketPrice().parse(
                        jsonArrTicketPrices.getJSONObject(i));
                arrTicketPrice.add(ticketPrice);
            }

            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

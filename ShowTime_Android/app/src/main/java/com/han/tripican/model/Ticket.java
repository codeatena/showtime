package com.han.tripican.model;

import org.json.JSONObject;

/**
 * Created by COREI3 on 2/6/2015.
 */
public class Ticket {

    public int id;
    public int unit_price;
    public int quantity;
    public TicketCategory ticketCategory;

    public Ticket parse(JSONObject jsonObj) {
        try {
            id = jsonObj.getInt("id");
            unit_price = jsonObj.getInt("unit_price");
            quantity = jsonObj.getInt("quantity");
            ticketCategory = new TicketCategory().parse(jsonObj.getJSONObject("category"));
            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

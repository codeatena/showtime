package com.han.tripican.model;

import org.json.JSONObject;

/**
 * Created by COREI3 on 2/6/2015.
 */
public class TicketCategory {

    public int id;
    public String name;
    public String description;
    public Object parent;

    public TicketCategory parse(JSONObject jsonObj) {
        try {
            id = jsonObj.getInt("id");
            name = jsonObj.getString("name");
            description = jsonObj.getString("description");
            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

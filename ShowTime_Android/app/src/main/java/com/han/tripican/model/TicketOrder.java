package com.han.tripican.model;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by COREI3 on 2/6/2015.
 */
public class TicketOrder {
    public int id;
    public String created_on;
    public int checkin_status;
    public Object reserve_expiry;
    public String phone;
    public String gift_owner_name;
    public String gift_owner_email;
    public ArrayList<Ticket> arrTickets;
    public int total_cost;
    public int total_due;
    public String confirmation_code;
    public Movie movie;
    public Cinema cinema;
    public Transaction transaction;
    public User user;
    public ShowTime showTime;

    public TicketOrder parse(JSONObject jsonObj) {

        try {
            id = jsonObj.getInt("id");
//            created_on = jsonObj.getString("created_on");
            checkin_status = jsonObj.getInt("checkin_status");
//            reserve_expiry = jsonObj.get("reserve_expiry");
//            phone = jsonObj.getString("phone");
//            gift_owner_name = jsonObj.getString("gift_owner_name");
//            gift_owner_email = jsonObj.getString("gift_owner_email");

            arrTickets = new ArrayList<Ticket>();
            JSONObject jsonTickets = jsonObj.getJSONObject("tickets");
            Iterator<String> strKeys = jsonTickets.keys();
            while(strKeys.hasNext()){
                String key = strKeys.next();
                Ticket ticket = new Ticket().parse(jsonTickets.getJSONObject(key));
                arrTickets.add(ticket);
            }
            total_cost = jsonObj.getInt("total_cost");
//            total_due = jsonObj.getInt("total_due");
//            confirmation_code = jsonObj.getString("confirmation_code");

            movie = new Movie().parse(jsonObj.getJSONObject("movie"));
            cinema = new Cinema().parse(jsonObj.getJSONObject("cinema"));
//            transaction = new Transaction().parse(jsonObj.getJSONObject("transaction"));
            user = new User().parse(jsonObj.getJSONObject("user"));
            showTime = new ShowTime().parse(jsonObj.getJSONObject("showtime"));

            return this;
        } catch (Exception e) {
            return null;
        }
    }
}

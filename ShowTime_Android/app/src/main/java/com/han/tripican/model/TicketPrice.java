package com.han.tripican.model;

import org.json.JSONObject;

/**
 * Created by COREI3 on 2/6/2015.
 */
public class TicketPrice {

    int price_amount;
    TicketCategory ticketCategory;

    public TicketPrice parse(JSONObject jsonObj) {
        try {
            price_amount = jsonObj.getInt("price_amount");
            ticketCategory = new TicketCategory().parse(jsonObj.getJSONObject("category"));
            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}



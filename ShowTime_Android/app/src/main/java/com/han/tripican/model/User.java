package com.han.tripican.model;

import org.json.JSONObject;

/**
 * Created by COREI3 on 2/6/2015.
 */
public class User {

    public int id;
    public String email;
    public String first_name;
    public String last_name;
    public int contact_phone_cc;
    public String contact_phone;
    public String created_on;
    public int address_state_id;
    public String address_city;

    public User parse(JSONObject jsonObj) {
        try {
            id = jsonObj.getInt("id");
            email = jsonObj.getString("email");
            first_name = jsonObj.getString("first_name");
            last_name = jsonObj.getString("last_name");
            contact_phone_cc = jsonObj.getInt("contact_phone_cc");
            contact_phone = jsonObj.getString("contact_phone");
            created_on = jsonObj.getString("created_on");
            address_state_id = jsonObj.isNull("address_state_id") ? 0 : jsonObj.getInt("address_state_id");
            address_city = jsonObj.getString("address_city");

            return this;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

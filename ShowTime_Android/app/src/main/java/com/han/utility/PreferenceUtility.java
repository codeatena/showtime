package com.han.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.han.api.OAuthToken;
import com.han.tripican.App;
import com.han.tripican.model.Cinema;

public class PreferenceUtility {
	
	public final String PREF_NAME = "tripican_pref";
	
	public SharedPreferences sharedPreferences;
	public Editor prefEditor;
	
	public static String PREFERENCE_KEY_OAUTH_TOKEN = "pref_key_oauth_token";
	public static String PREFERENCE_KEY_OAUTH_TOKEN_SECRET = "pref_key_oauth_token_secret";
	public static String PREFERENCE_KEY_EMAIL = "pref_key_email";
	public static String PREFERENCE_KEY_PASSWORD = "pref_key_password";
	public static String PREFERENCE_KEY_CONFIG_CINEMA_ID = "pref_key_config_cinema_id";
	public static String PREFERENCE_KEY_CONFIG_CINEMA_CENTER_NAME = "pref_key_config_cinema_center_name";
	
	public static PreferenceUtility instance = null;
	
	public static PreferenceUtility getInstance() {
		
		if (instance == null) {
			instance = new PreferenceUtility();
		}
		
		return instance;
	}
	
	public PreferenceUtility() {
        sharedPreferences = App.getInstance().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        prefEditor = sharedPreferences.edit();
	}
	
	public void saveOAuthToken(OAuthToken oauthToken) {
		prefEditor.putString(PREFERENCE_KEY_OAUTH_TOKEN, oauthToken.getToken());
		prefEditor.putString(PREFERENCE_KEY_OAUTH_TOKEN_SECRET, oauthToken.getTokenSecret());
		prefEditor.commit();
	}
	
	public OAuthToken getOAuthToken() {
		String strOAuthToken = sharedPreferences.getString(PREFERENCE_KEY_OAUTH_TOKEN, "");
		String strOAuthTokenSecret = sharedPreferences.getString(PREFERENCE_KEY_OAUTH_TOKEN_SECRET, "");
		OAuthToken oauthToken = new OAuthToken(strOAuthToken, strOAuthTokenSecret);
		
		return oauthToken;
	}

    public void clearAllInf() {
        prefEditor.putString(PREFERENCE_KEY_EMAIL, "");
        prefEditor.putString(PREFERENCE_KEY_PASSWORD, "");
        prefEditor.putInt(PREFERENCE_KEY_CONFIG_CINEMA_ID, -1);
        prefEditor.putString(PREFERENCE_KEY_CONFIG_CINEMA_CENTER_NAME, "");
        prefEditor.commit();
    }
	
	public void saveUserInf(String email, String password) {
		prefEditor.putString(PREFERENCE_KEY_EMAIL, email);
		prefEditor.putString(PREFERENCE_KEY_PASSWORD, password);
		prefEditor.commit();
	}
	
	public void saveCinemaConfig(Cinema cinema) {
		prefEditor.putInt(PREFERENCE_KEY_CONFIG_CINEMA_ID, cinema.id);
		prefEditor.putString(PREFERENCE_KEY_CONFIG_CINEMA_CENTER_NAME, cinema.centre_name);
		prefEditor.commit();
	}

    public boolean isSavedUserInf() {
        String email = sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_EMAIL, "");
        if (email.equals("")) {
            return false;
        }
        return true;
    }

    public boolean isSavedCinemaConfig() {
        int cinemaID = sharedPreferences.getInt(PreferenceUtility.PREFERENCE_KEY_CONFIG_CINEMA_ID, -1);
        if (cinemaID == -1) {
            return false;
        }
        return true;
    }

}
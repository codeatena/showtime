package com.han.zxing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

import com.han.tripican.InvalidActivity;
import com.han.tripican.R;
import com.han.tripican.ValidActivity;
import com.han.tripican.api.parseresult.VerifyParseResult;

public class SimpleScannerFragmentActivity extends ActionBarActivity {

    Button btnCancel;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_simple_scanner_fragment);

        initWidget();
        initValue();
        initEvent();
    }

    public void successVerify(VerifyParseResult verifyParseResult) {
        if (verifyParseResult.ticket_order.checkin_status == 0) {
            ValidActivity.ticketOrder = verifyParseResult.ticket_order;
            startActivity(new Intent(this, ValidActivity.class));
        }
        if (verifyParseResult.ticket_order.checkin_status == 1) {
            if (verifyParseResult.statusPass == VerifyParseResult.PASSED_CHECK_IN) {
                ValidActivity.ticketOrder = verifyParseResult.ticket_order;
                startActivity(new Intent(this, ValidActivity.class));
            } else {
                InvalidActivity.ticketOrder = verifyParseResult.ticket_order;
                startActivity(new Intent(this, InvalidActivity.class));
            }
        }
        finish();
    }

    private void initWidget() {
        btnCancel = (Button) findViewById(R.id.cancel_button);
    }

    private void initValue() {

    }

    private void initEvent() {
        btnCancel.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}